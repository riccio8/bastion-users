﻿namespace Bastion.Users.Extensions
{
    using Bastion.Users.Constants;
    using Bastion.Users.Models;

    public static class UserProfileExtensions
    {
        public static string ToName(this UserProfile profile)
        {
            if(profile == null)
                return "";
            return profile .FirstName + " " + profile.LastName;
        }

        public static string GetLanguageIdOrDefault(this UserProfile profile)
            => profile.LanguageId ?? "en";

        public static bool IsCorporateAccount(this UserProfile profile) => profile.ClientTypeId == ClientTypeConstants.Corporate;

        public static bool IsIndividualAccount(this UserProfile profile) => profile.ClientTypeId == ClientTypeConstants.Individual;
    }
}
