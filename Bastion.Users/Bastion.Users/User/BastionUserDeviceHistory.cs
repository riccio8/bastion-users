﻿namespace Bastion.Authorization
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Bastion user device history.
    /// </summary>
    public class BastionUserDeviceHistory
    {
        /// <summary>
        /// Id.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        /// <summary>
        /// Device id.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Device.
        /// </summary>
        public virtual BastionUserDevice Device { get; set; }

        /// <summary>
        /// Path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Is success.
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Ts.
        /// </summary>
        public DateTime Ts { get; set; }

        /// <summary>
        /// Ip deny access.
        /// </summary>
        public string IpDeny { get; set; }
    }
}
