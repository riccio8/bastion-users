﻿namespace Bastion.Users.Constants
{
    public static class AlertGroupConstants
    {
        public const string System = "SYSTEM";
        public const string Security = "SECURITY";
        public const string BankBalance = "BANK_BALANCE";
        public const string BankPayments = "BANK_PAYMENTS";
    }
}
