﻿namespace Bastion.Users.Constants
{
    public static class RoleConstants
    {
        public const string Admin = "Admin";
        public const string Operator = "Operator";
        public const string Clerk = "Clerk";
        public const string Controller = "Controller";
        public const string Regulator = "Regulator";
        public const string Broker = "Broker";
        public const string Client = "Client";
        public const string Corporate = "Corporate";
        public const string CorporateWaitForApproval = "WaitForApproval";

        public const string AdminOrCorporate = "Admin, Corporate";
        public const string AdminOrRegulator = "Admin, Regulator";
        public const string ClerkrOrOperatorOrAdmin = "Clerk, Operator, Admin";
        public const string AdminOrCorporateOrBroker = "Admin, Corporate, Broker";
        public const string ControllerOrOperatorOrAdmin = "Controller, Operator, Admin";
        public const string ClerkOrControllerOrOperatorOrAdmin = "Clerk, Controller, Operator, Admin";
        public const string ClerkOrControllerOrOperatorOrAdminOrRegulator = "Clerk, Controller, Operator, Admin, Regulator";
    }
}
