﻿namespace Bastion.Users.Constants
{
    public static class ClientTypeConstants
    {
        public const string Corporate = "Corporate";

        public const string Individual = "Individual";
    }
}
