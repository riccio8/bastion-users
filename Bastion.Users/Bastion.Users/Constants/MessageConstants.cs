﻿namespace Bastion.Users.Constants
{
    public static class MessageConstants
    {
        public const string KycPending = "KYC_PENDING";
        public const string kycRejected = "KYC_REJECTED";
        public const string KycApproved = "KYC_APPROVED";

        public const string DepositPending = "DEPOSIT_PENDING";
        public const string DepositRejected = "DEPOSIT_REJECTED";
        public const string DepositConfirmed = "DEPOSIT_CONFIRMED";

        public const string WithdrawPending = "WITHDRAW_PENDING";
        public const string WithdrawRejected = "WITHDRAW_REJECTED";
        public const string WithdrawConfirmed = "WITHDRAW_CONFIRMED";

        public const string Welcome = "WELCOME";
        public const string WelcomeCorporate = "WELCOME-CORPORATE";

        public const string LoginSuccess = "LOGIN-SUCCESS";
        public const string TwoFactorCode = "TWO-FACTOR-CODE";

        public const string EmailConfirm = "EMAIL-CONFIRM";
        public const string AccountVerified = "ACCOUNT_VERIFIED";

        public const string PasswordReset = "PASSWORD-RESET";
        public const string PasswordResetSuccess = "PASSWORD-RESET-SUCCESS";

        public const string DeviceConfirm = "DEVICE-CONFIRM";
        public const string DeviceConfirmSuccess = "DEVICE-CONFIRM-SUCCESS";

        public const string NotificationEmail = "NOTIFICATION-DEPOSIT";
    }
}
