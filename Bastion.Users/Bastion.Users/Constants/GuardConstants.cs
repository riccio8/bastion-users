﻿namespace Bastion.Users.Constants
{
    public static class GuardConstants
    {
        public const string PasswordChange = "UPC";

        public const string DeviceEnable = "UDE";
        public const string DeviceDelete = "UDR";
        public const string DeviceDisable = "UDD";
        public const string DeviceNewEnable = "UNE";
        public const string DeviceNewDisable = "UND";        

        public const string TwoFactorDisable = "U2D";
        public const string TwoFactorEnable = "U2E";

        public const string WalletMoneyPay = "WMP";
        public const string WalletMoneySend = "WMS";        
        public const string WalletMoneyRequest = "WMR";

        public const string NotificationDepositApprove = "NWA";
        public const string NotificationWihdrawApprove = "NDA";
    }
}
