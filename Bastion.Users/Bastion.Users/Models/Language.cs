﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class Language
    {
        public Language()
        {
            UserProfile = new HashSet<UserProfile>();
        }

        public string LanguageId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserProfile> UserProfile { get; set; }
    }
}
