﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class MyClient
    {
        public string UserId { get; set; }
        public string ClientId { get; set; }

        public virtual UserProfile Client { get; set; }
    }
}
