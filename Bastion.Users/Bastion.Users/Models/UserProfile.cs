﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class UserProfile
    {
        public UserProfile()
        {
            AlertUser = new HashSet<AlertUser>();
            File = new HashSet<File>();
            Guard = new HashSet<Guard>();
            MyClient = new HashSet<MyClient>();
            MyContact = new HashSet<MyContact>();
        }

        public string UserId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FeeStructureId { get; set; }
        public DateTime? PromoFeeExpireDate { get; set; }
        public string ClientTypeId { get; set; }
        public string FeeRegionId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateOrProvince { get; set; }
        public string CountryId { get; set; }
        public string PostCode { get; set; }
        public DateTime? RegistrationTs { get; set; }
        public string Location { get; set; }
        public bool? SendMoneyReference { get; set; }
        public string LanguageId { get; set; }
        public string TimeZone { get; set; }
        public string Nationality { get; set; }
        public string Ssn { get; set; }
        public string Sex { get; set; }
        public string Inn { get; set; }
        public string Citizenship { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string RegistrationType { get; set; }
        public string Nickname { get; set; }
        public string KycIdentityStatus { get; set; }
        public string KycAddressStatus { get; set; }
        public bool KycCompleted { get; set; }
        public string KycIdentityReference { get; set; }
        public DateTime? KycIdentityReferenceCreationTime { get; set; }
        public string KycAddressReference { get; set; }
        public DateTime? KycAddressReferenceCreationTime { get; set; }
        public string KycAddressLastCountry { get; set; }
        public string KycAddressLastDocumentType { get; set; }
        public string AvatarFileId { get; set; }
        public int? VerificationLevelId { get; set; }
        public string DimmFeeStructureId { get; set; }
        public bool ClientApproved { get; set; }
        public bool Visible { get; set; }
        public string UnlimitedStatus { get; set; }

        public virtual Country Country { get; set; }
        public virtual Language Language { get; set; }
        public virtual VerificationLevel VerificationLevel { get; set; }
        public virtual Alias Alias { get; set; }
        public virtual ICollection<AlertUser> AlertUser { get; set; }
        public virtual ICollection<File> File { get; set; }
        public virtual ICollection<Guard> Guard { get; set; }
        public virtual ICollection<MyClient> MyClient { get; set; }
        public virtual ICollection<MyContact> MyContact { get; set; }
    }
}
