﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class Country
    {
        public Country()
        {
            UserProfile = new HashSet<UserProfile>();
        }

        public string CountryId { get; set; }
        public string Iso3 { get; set; }
        public string Name { get; set; }
        public int PhoneCode { get; set; }
        public string RegionId { get; set; }
        public string FeeRegionId { get; set; }

        public virtual CountryRegion Region { get; set; }
        public virtual ICollection<UserProfile> UserProfile { get; set; }
    }
}
