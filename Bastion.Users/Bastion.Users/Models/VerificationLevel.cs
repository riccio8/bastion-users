﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class VerificationLevel
    {
        public VerificationLevel()
        {
            UserProfile = new HashSet<UserProfile>();
        }

        public int VerificationLevelId { get; set; }
        public string ShortTitle { get; set; }
        public decimal MaxLimit { get; set; }
        public string TitleApproved { get; set; }
        public string TitleRejected { get; set; }
        public string TitlePending { get; set; }
        public string TitleNone { get; set; }
        public string DescriptionApproved { get; set; }
        public string DescriptionRejected { get; set; }
        public string DescriptionPending { get; set; }
        public string DescriptionNone { get; set; }
        public decimal? MaxLimitDimm { get; set; }
        public decimal? MaxLimitMigom { get; set; }

        public virtual ICollection<UserProfile> UserProfile { get; set; }
    }
}
