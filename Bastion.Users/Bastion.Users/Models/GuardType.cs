﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class GuardType
    {
        public GuardType()
        {
            Guard = new HashSet<Guard>();
        }

        public string GuardTypeId { get; set; }
        public string Name { get; set; }
        public short SortOrder { get; set; }
        public bool DefaultTwoFactorRequired { get; set; }
        public bool ShowMigom { get; set; }
        public bool ShowDimm { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Guard> Guard { get; set; }
    }
}
