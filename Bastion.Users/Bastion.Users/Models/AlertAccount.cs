﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class AlertAccount
    {
        public string AccountId { get; set; }
        public string AlertId { get; set; }
        public bool Email { get; set; }
        public bool Sms { get; set; }
        public bool Push { get; set; }
        public decimal? Value { get; set; }

        public virtual Alert Alert { get; set; }
    }
}
