﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class File
    {
        public string UserId { get; set; }
        public string FileId { get; set; }
        public string FileType { get; set; }
        public string FileExtension { get; set; }
        public DateTime? UploadTs { get; set; }
        public byte[] FileData { get; set; }

        public virtual UserProfile User { get; set; }
    }
}
