﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class CountryRegion
    {
        public CountryRegion()
        {
            Country = new HashSet<Country>();
        }

        public string RegionId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Country> Country { get; set; }
    }
}
