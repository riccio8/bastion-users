﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class MyContact
    {
        public string UserId { get; set; }
        public string MyContactUserId { get; set; }
        public bool IsFavorite { get; set; }
        public DateTime? LastSentMoney { get; set; }
        public DateTime? LastRequestMoney { get; set; }

        public virtual UserProfile MyContactUser { get; set; }
    }
}
