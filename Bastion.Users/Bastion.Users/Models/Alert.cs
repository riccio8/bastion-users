﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class Alert
    {
        public Alert()
        {
            AlertAccount = new HashSet<AlertAccount>();
            AlertUser = new HashSet<AlertUser>();
        }

        public string AlertId { get; set; }
        public string GroupId { get; set; }
        public string FrequencyId { get; set; }
        public string Name { get; set; }
        public string RoleId { get; set; }
        public string ExtraId { get; set; }
        public bool? DefaultSendEmail { get; set; }
        public bool? DefaultSendSms { get; set; }
        public bool? DefaultSendPush { get; set; }
        public bool? ShowMigom { get; set; }
        public bool? ShowDimm { get; set; }
        public short? SortOrder { get; set; }
        public string MessageId { get; set; }
        public string Description { get; set; }
        public bool HasValue { get; set; }
        public string SmsText { get; set; }
        public string EmailText { get; set; }

        public virtual Extra Extra { get; set; }
        public virtual Frequency Frequency { get; set; }
        public virtual Group Group { get; set; }
        public virtual ICollection<AlertAccount> AlertAccount { get; set; }
        public virtual ICollection<AlertUser> AlertUser { get; set; }
    }
}
