﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class Alias
    {
        public string UserId { get; set; }
        public string AliasName { get; set; }
        public string NormalizedAlias { get; set; }
        public bool Enabled { get; set; }

        public virtual UserProfile User { get; set; }
    }
}
