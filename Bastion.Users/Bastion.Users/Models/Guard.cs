﻿using System;
using System.Collections.Generic;

namespace Bastion.Users.Models
{
    public partial class Guard
    {
        public string UserId { get; set; }
        public string GuardTypeId { get; set; }
        public bool TwoFactor { get; set; }

        public virtual GuardType GuardType { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
