﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Authenticator two factor ViewModel
    /// </summary>
    public class AuthenticatorTwoFactorViewModel
    {
        /// <summary>
        /// Authenticator key.
        /// </summary>
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// 2FA code.
        /// </summary>
        [Required]
        public string TwoFactorCode { get; set; }
    }
}