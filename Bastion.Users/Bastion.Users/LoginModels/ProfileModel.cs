﻿namespace Bastion.Authorization
{
    /// <summary>
    /// Profile model.
    /// </summary>
    public class ProfileModel
    {
        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// First name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Avatar id.
        /// </summary>
        public string AvatarId { get; set; }

        public string KycIdentityStatus { get; set; }

        public string KycAddressStatus { get; set; }

        /// <summary>
        /// Client type.
        /// </summary>
        public string ClientType { get; set; }

        /// <summary>
        /// Client approved.
        /// </summary>
        public bool ClientApproved { get; set; }
    }
}