﻿namespace Bastion.Authorization
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Register base
    /// </summary>
    public class RegisterBase
    {
        /// <summary>
        /// Email.
        /// </summary>
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// FirstName.
        /// </summary>
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "FirstName")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Invalid first name. Only letters are allowed.")]
        public string FirstName { get; set; }

        /// <summary>
        /// LastName.
        /// </summary>
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "LastName")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Invalid last name. Only letters are allowed.")]
        public string LastName { get; set; }

        /// <summary>
        /// Phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Address1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Address2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Post code.
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// Country id.
        /// </summary>
        public string CountryId { get; set; }
    }
}