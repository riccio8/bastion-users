﻿namespace Bastion.Users.Services
{

    using Bastion.Accounts.Models;
    using Bastion.Authorization;
    using Bastion.PubSub;
    using Bastion.Senders.EmailSender.Abstractions;
    using Bastion.Senders.EmailSender.Extensions;
    using Bastion.Senders.FirebaseSender.Abstractions;
    using Bastion.Senders.FirebaseSender.Extensions;
    using Bastion.Senders.Messages;
    using Bastion.Senders.SmsSender.Abstractions;
    using Bastion.Senders.SmsSender.Extensions;
    using Bastion.Users.Constants;
    using Bastion.Users.Extensions;
    using Bastion.Users.Models;
    using Bastion.Users.Services;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Alert service.
    /// </summary>
    public class SystemAlertService
    {
        private readonly UsersRepository usersRepository;
        private readonly ISmsSender smsSender;
        private readonly IEmailSender emailSender;
        private readonly IFirebaseSender firebaseSender;
        private readonly IMessagesService messagesService;
        private readonly UserManager<BastionUser> userManager;

        /// <summary>
        /// Initializes a new instance of <see cref="SystemAlertService"/>.
        /// </summary>
        public SystemAlertService(
            ISmsSender smsSender,
            IEmailSender emailSender,
            IFirebaseSender firebaseSender,
            IMessagesService messagesService,
            UserManager<BastionUser> userManager,
            UsersRepository usersRepository)
        {
            this.usersRepository = usersRepository;
            this.smsSender = smsSender;
            this.emailSender = emailSender;
            this.userManager = userManager;
            this.messagesService = messagesService;
            this.firebaseSender = firebaseSender;
        }

        private async Task<AlertUser> GetAlertAsync(string alertId)
        {
            var alertUser = await usersRepository.GetUserAlerts().FirstOrDefaultAsync(x => x.AlertId == alertId);
            if (alertUser != null)
                return alertUser;
            else
                return default;
        }

        private async Task SendAlertAsync<T>(BastionUser user, AlertUser alertUser, string languageId, T data) where T : class
        {
            if (alertUser.Email && user.EmailConfirmed)
            {
                if (alertUser.Email)
                {
                    await emailSender.SendEmailAsync(user.Email, messagesService, alertUser.Alert.MessageId, languageId, data);
                }
                if (alertUser.Push)
                {
                    await firebaseSender.SendMessageAsync(user.Email, messagesService, alertUser.Alert.MessageId, languageId, data);
                }
                
            }
            if (alertUser.Sms && user.PhoneNumberConfirmed)
            {
                await smsSender.SendSmsAsync(user.PhoneNumber, messagesService, alertUser.Alert.MessageId, languageId, data);
            }
            //Удалить после тестирования всех алертов
            if (alertUser.AlertId != "LOGIN")
            {
                await emailSender.SendEmailAsync("dsazonoff@curlybr.com", messagesService, alertUser.Alert.MessageId, languageId, data);
                await emailSender.SendEmailAsync("aporozhniakov@curlybr.com", messagesService, alertUser.Alert.MessageId, languageId, data);
                await smsSender.SendSmsAsync("+19174979150", messagesService, alertUser.Alert.MessageId, languageId, data);
            }
        }

        public async Task SendAlertAsync(string userId, string subject, string message)
        {
            BastionUser user = await userManager.FindByIdAsync(userId) ?? throw new Exception($"User '{userId}' is not exist.");
            if (user.EmailConfirmed)
            {
                await emailSender.SendEmailAsync(user.Email, subject, message, message);
            }
            if (user.PhoneNumberConfirmed)
            {
                await smsSender.SendSmsAsync(user.PhoneNumber, message);
            }
        }

        //void Replace(StringBuilder sb, string template)
        //{
        //    // {UserName}, совершён вход в {SiteName} {Now}. Если вход произведён не вами, обратитесь в контактный центр.
        //    sb.Append(template
        //                      .Replace("{Now}", DateTime.Now.ToString("HH:mm:ss"))
        //                      .Replace("{Time}", DateTime.Now.ToString("HH:mm:ss"))
        //                      .Replace("{SiteName}", DateTime.Now.ToString("HH:mm:ss"))
        //                      .Replace("{UserName}", DateTime.Now.ToString("HH:mm:ss"))
        //                      );
        //}

        /// <summary>
        /// Send alert when login success.
        /// </summary>
        /// <param name="user">User.</param>
        /// <returns>The task.</returns>
        public async Task SendLoginSuccessAsync(BastionUser user)
        {
            var alert = await GetAlertAsync("LOGIN");
            if (alert != null)
            {
                var profile = await usersRepository.GetUserProfile(user.Id);
                await SendAlertAsync(user, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Now = DateTime.UtcNow,
                    UserName = profile.ToName()
                });
            }
        }

        /// <summary>
        /// Send alert to clerk when corporate created
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="userName">User name.</param>
        /// <param name="phone">Phone.</param>
        /// <returns>The task.</returns>
        public async Task SendCorpUserCreatedAsync(string email, string userName, string phone)
        {
            var alert = await GetAlertAsync("CORP_USER_CREATED");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Email = email,
                    UserName = userName,
                    Phone = phone
                });
            }
        }

        /// <summary>
        /// Send alert to controller when corporate register
        /// </summary>
        /// <param name="email">Email.</param>
        /// <returns>The task.</returns>
        public async Task SendCorpUserNeedReviewAsync(string email)
        {
            var alert = await GetAlertAsync("CORP_USER_NEEDREVIEW");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Email = email
                });
            }
        }

        /// <summary>
        /// Send alert to clerk when corporate rejected
        /// </summary>
        /// <param name="email">Email.</param>
        /// <returns>The task.</returns>
        public async Task SendCorpUserRejectedAsync(string email)
        {
            var alert = await GetAlertAsync("CORP_USER_REJECTED");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Email = email
                });
            }
        }

        /// <summary>
        /// Send alert to user when corporate approved
        /// </summary>
        /// <param name="email">Email.</param>
        /// <returns>The task.</returns>
        public async Task SendCorpUserApprovedAsync(string email)
        {
            var alert = await GetAlertAsync("CORP_USER_APPROVED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                });
            }
        }

        /// <summary>
        /// Send alert to user when clerk rejected document
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="userName">User name.</param>
        /// <param name="document">Document.</param>
        /// <param name="reason">Reason.</param>
        /// <returns>The task.</returns>
        public async Task SendDocumentRejectedAsync(string email, string userName, string document, string reason)
        {
            var alert = await GetAlertAsync("DOCUMENT_REJECTED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    UserName = userName,
                    Document = document,
                    Reason = reason
                });
            }
        }

        /// <summary>
        /// Send alert to user when clerk rejected document
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="document">Document.</param>
        /// <param name="userName"></param>
        /// <returns>The task.</returns>
        public async Task SendDocumentApprovedAsync(string email, string document, string userName)
        {
            var alert = await GetAlertAsync("DOCUMENT_APPROVED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    UserName = userName,
                    Document = document
                });
            }
        }

        /// <summary>
        /// Send alert to clerk when user updated document
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="document">Document.</param>
        /// <returns>The task.</returns>
        public async Task SendDocumentUpdatedAsync(string userName, string document)
        {
            var alert = await GetAlertAsync("DOCUMENT_UPDATED");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    UserName = userName,
                    Document = document
                });
            }
        }

        /// <summary>
        /// Send alert to operator when new mega created
        /// </summary>
        /// <param name="megaId">New mega ID.</param>
        /// <returns>The task.</returns>
        public async Task SendNewMegaCreatedAsync(string megaId)
        {
            var alert = await GetAlertAsync("MEGA_CREATED");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    MegaId = megaId
                });
            }
        }

        /// <summary>
        /// Send alert to user when operator approve deposit
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="value">Deposit amount.</param>
        /// <param name="currency">Currency.</param>
        /// <returns>The task.</returns>
        public async Task SendDepositApprovedAsync(string email, string value, string currency)
        {
            var alert = await GetAlertAsync("DEPOSIT_APPROVED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Value = value,
                    Currency = currency
                });
            }
        }

        /// <summary>
        /// Send alert to user when operator reject deposit
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="value">Deposit amount.</param>
        /// <param name="currency">Currency.</param>
        /// <returns>The task.</returns>
        public async Task SendDepositRejectedAsync(string email, string value, string currency)
        {
            var alert = await GetAlertAsync("DEPOSIT_REJECTED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Value = value,
                    Currency = currency
                });
            }
        }

        /// <summary>
        /// Send alert to operator when new deposit requested
        /// </summary>
        /// <param name="depositId">Deposit ID.</param>
        /// <param name="value">Deposit amount.</param>
        /// <param name="currency">Currency.</param>
        /// <returns>The task.</returns>
        public async Task SendDepositRequestAsync(string depositId, string value, string currency)
        {
            var alert = await GetAlertAsync("DEPOSIT_REQUEST");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    DepositId = depositId,
                    Value = value,
                    Currency = currency
                });
            }
        }

        /// <summary>
        /// Send alert to user when operator approve withdraw
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="value">Withdraw amount.</param>
        /// <param name="currency">Currency.</param>
        /// <returns>The task.</returns>
        public async Task SendWithdrawApprovedAsync(string email, string value, string currency)
        {
            var alert = await GetAlertAsync("WITHDRAW_APPROVED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Value = value,
                    Currency = currency
                });
            }
        }

        /// <summary>
        /// Send alert to user when operator reject withdraw
        /// </summary>
        /// <param name="email">Email.</param>
        /// <param name="value">Withdraw amount.</param>
        /// <param name="currency">Currency.</param>
        /// <returns>The task.</returns>
        public async Task SendWithdrawRejectedAsync(string email, string value, string currency)
        {
            var alert = await GetAlertAsync("WITHDRAW_REJECTED");
            if (alert != null)
            {
                var receiver = await userManager.FindByEmailAsync(email);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    Value = value,
                    Currency = currency
                });
            }
        }

        /// <summary>
        /// Send alert to operator when new withdraw requested
        /// </summary>
        /// <param name="depositId">Deposit ID.</param>
        /// <param name="value">Withdraw amount.</param>
        /// <param name="currency">Currency.</param>
        /// <returns>The task.</returns>
        public async Task SendWithdrawRequestAsync(string depositId, string value, string currency)
        {
            var alert = await GetAlertAsync("WITHDRAW_REQUEST");
            if (alert != null)
            {
                var receiver = await userManager.FindByIdAsync(alert.UserId);
                if (receiver == null)
                {
                    return;
                }

                var profile = await usersRepository.GetUserProfile(receiver.Id);
                await SendAlertAsync(receiver, alert, profile.GetLanguageIdOrDefault(), new
                {
                    DepositId = depositId,
                    Value = value,
                    Currency = currency
                });
            }
        }

        ///// <summary>
        ///// Send a login alert from a new device.
        ///// </summary>
        ///// <param name="user">User.</param>
        ///// <param name="device">Device.</param>
        ///// <returns>The task.</returns>
        //public async Task SendLoginNewDeviceAsync(BastionUser user, BastionUserDevice device)
        //{
        //    var alert = await GetAlertAsync(user, AlertConstants.LoginNewDevice);
        //    if (alert != null)
        //    {
        //        var profile = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == user.Id);
        //        await SendAlertAsync(user, alert, profile.GetLanguageIdOrDefault(), new
        //        {
        //            Now = DateTime.UtcNow,
        //            UserName = profile.ToName(),

        //            device.City,
        //            device.Country,
        //            device.Browser,
        //            IpAddress = device.Ip,
        //        });
        //    }
        //}

        // Send alert when deposit success

        /// <summary>
        /// Send alert when request money.
        /// </summary>
        /// <param name="userIdTo">User id to.</param>
        /// <param name="userIdFrom">User id from.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="currencyId">Currency id.</param>
        /// <param name="description">Description.</param>
        /// <param name="notificationId">Ntification id.</param>
        /// <returns>The task.</returns>
        public async Task SendRequestMoneyAsync(string userIdTo, string userIdFrom, decimal amount, string currencyId, string description, string notificationId)
        {
            var userTo = await userManager.FindByIdAsync(userIdTo);
            var userFrom = await userManager.FindByIdAsync(userIdFrom);

            if (userTo != null && userTo.EmailConfirmed || userTo.PhoneNumberConfirmed)
            {
                //        var alertType = await repository.GetFirstEntityAsync<AlertType>(x
                //            => x.AlertTypeId == AlertConstants.MoneyRequest);

                // TODO: запрос денег должен быть для Пользователя? или сразу на конкретный счёт пользователя?
                // пока не понятно

                //var alertUser = await repository.GetFirstEntityAsync<AlertUser>(x
                //    => x.AlertId == AlertConstants.MoneyRequest);

                //        if (alertType != null && !string.IsNullOrEmpty(alertType.MessageId))
                //        {
                //            var alert = await repository.GetEntity<Alert>(x
                //                => x.AlertTypeId == alertType.AlertTypeId && x.UserId == userTo.Id);

                //            if (alert != null)
                //            {
                //                var profileTo = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == userTo.Id);
                //                var profileFrom = await repository.GetFirstEntityAsync<UserProfile>(x => x.UserId == userFrom.Id);

                //                if (alert.Email && userTo.EmailConfirmed)
                //                {
                //                    await emailSender.SendEmailAsync(userTo.Email, messagesService, alertType.MessageId, profileTo.LanguageId, new
                //                    {
                //                        Amount = amount,
                //                        Currency = currencyId,
                //                        Now = DateTime.UtcNow,
                //                        Description = description,
                //                        UserName = profileTo.ToName(),
                //                        NotificationId = notificationId,
                //                        UserNameFrom = profileFrom.ToName(),
                //                        UserEmailFrom = profileFrom.Email,
                //                    });
                //                }
                //                if (alert.Sms && userTo.PhoneNumberConfirmed)
                //                {
                //                    await smsSender.SendSmsAsync(userTo.PhoneNumber, messagesService, alertType.MessageId, profileTo.LanguageId, new
                //                    {
                //                        Amount = amount,
                //                        Currency = currencyId,
                //                        Now = DateTime.UtcNow,
                //                        UserNameFrom = profileFrom.ToName(),
                //                        UserEmailFrom = profileFrom.Email
                //                    });
                //                }
                //            }
                //        }
            }
        }

        #region -- подтверждение почты, смена паролей и прочие системные сообщения --
        /// <summary>
        /// Send email welcome.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailWelcomeAsync(BastionUser user)
        {
            var profile = await usersRepository.GetUserProfile(user.Id);

            //if (profile.IsCorporateAccount())
            //{
            //    var corporateDocuments = await onBoardRepository.GetDocumentTypesAsync();

            //    await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.WelcomeCorporate, profile.GetLanguageIdOrDefault(), new
            //    {
            //        UserName = profile.ToName(),
            //        Documents = "<ul>" + string.Join(string.Empty, corporateDocuments.Select(x => $"<li><a href=\"{x.Url}\">{x.Title}</a></li>")) + "</ul>",
            //    });
            //}
            //else
            {
                await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.Welcome, profile.GetLanguageIdOrDefault(), new
                {
                    UserName = profile.ToName()
                });
            }
        }

        /// <summary>
        /// Send email confirmation.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailConfirmationAsync(BastionUser user)
        {
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
            var codeBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(code));

            var profile = await usersRepository.GetUserProfile(user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.EmailConfirm, profile.GetLanguageIdOrDefault(), new
            {
                UserId = user.Id,
                Code = codeBase64,
                UserName = profile.ToName()
            });
        }

        /// <summary>
        /// Send email password request.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailPasswordRequestAsync(BastionUser user)
        {
            var code = await userManager.GeneratePasswordResetTokenAsync(user);
            var codeBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(code));

            var profile = await usersRepository.GetUserProfile(user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.PasswordReset, profile.GetLanguageIdOrDefault(), new
            {
                UserId = user.Id,
                Code = codeBase64,
                UserName = profile.ToName()
            });
        }

        /// <summary>
        /// Send email password reset success.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailPasswordResetSuccessAsync(BastionUser user)
        {
            var profile = await usersRepository.GetUserProfile(user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.PasswordResetSuccess, profile.GetLanguageIdOrDefault(), new
            {
                UserName = profile.ToName()
            });
        }

        /// <summary>
        /// Send email two factor code.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="code">Two factor code</param>
        /// <returns>The task.</returns>
        public async Task SendEmailTwoFactorCodeAsync(BastionUser user, string code)
        {
            var profile = await usersRepository.GetUserProfile(user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.TwoFactorCode, profile.GetLanguageIdOrDefault(), new
            {
                UserName = profile.ToName(),
                Code = code
            });
        }

        /// <summary>
        /// Send sms two factor code.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="code">Two factor code</param>
        /// <returns>The task.</returns>
        public async Task SendSmsTwoFactorCodeAsync(BastionUser user, string code)
        {
            var profile = await usersRepository.GetUserProfile(user.Id);

            await smsSender.SendSmsAsync(user.PhoneNumber, messagesService, MessageConstants.TwoFactorCode, profile.GetLanguageIdOrDefault(), new
            {
                Code = code
            });
        }

        /// <summary>
        /// Send sms change phone number code.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="phoneNumber">Phone number.</param>
        /// <returns>The task.</returns>
        public async Task SendSmsChangePhoneNumberCodeAsync(BastionUser user, string phoneNumber)
        {
            var profile = await usersRepository.GetUserProfile(user.Id);

            var code = await userManager.GenerateChangePhoneNumberTokenAsync(user, phoneNumber);

            await smsSender.SendSmsAsync(phoneNumber, messagesService, MessageConstants.TwoFactorCode, profile.GetLanguageIdOrDefault(), new
            {
                Code = code
            });
        } 
        #endregion

        /// <summary>
        /// Send email for device confirmation.
        /// </summary>
        /// <param name="user">Bastion user.</param>
        /// <param name="device">Device.</param>
        /// <returns>The task.</returns>
        public async Task SendEmailDeviceConfirmationAsync(BastionUser user, BastionUserDevice device)
        {
            var code = await userManager.GenerateUserTokenAsync(user, "Default", "DeviceConfirmation");
            var codeBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(code));

            var profile = await usersRepository.GetUserProfile(user.Id);

            await emailSender.SendEmailAsync(user.Email, messagesService, MessageConstants.DeviceConfirm, profile.GetLanguageIdOrDefault(), new
            {
                UserId = user.Id,
                Code = codeBase64,
                UserName = profile.ToName(),

                device.City,
                device.Country,
                device.Browser,
                DeviceId = device.Id,
                IpAddress = device.Ip,
            });
        }
    }
}