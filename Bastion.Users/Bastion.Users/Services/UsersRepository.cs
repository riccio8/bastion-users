﻿namespace Bastion.Users.Services
{
    using Bastion.Helper;
    using Bastion.Users.Context;
    using Bastion.Users.Models;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public class UsersRepository //: Repository<UsersContext>
    {
        private readonly UsersContext context;

        public UsersRepository(UsersContext context) //: base(context)
        {
            this.context = context;
        }

        //public UsersRepository ()
        //{

        //}

        public async Task<UserProfile> GetUserProfile(string userId)
        {
            UserProfile userProfile = this.context.UserProfile.FirstOrDefault(x => x.UserId == userId);
            if (userProfile == null)
            {
                throw new System.Exception($"User profile for user '{userId}' is not exist. ({nameof(UsersRepository)}.{nameof(GetUserProfile)})");
            }

            return userProfile;
        }

        public async Task<IList<string>> GetAllProfilesIds()
        {
            return await this.context.UserProfile.Select(x => x.UserId).ToListAsync();
        }

        public async Task AddUserProfile(UserProfile userProfile)
        {
            await this.context.UserProfile.AddAsync(userProfile);
            await this.context.SaveChangesAsync();
        }

        public async Task<T> AddObjectToDb<T>(T objectToAdd)
        {
            await this.context.AddAsync(objectToAdd);
            await this.context.SaveChangesAsync();
            return objectToAdd;
        }

        public async Task UpdateObjectInDb(object objectToUpdate)
        {
            this.context.Update(objectToUpdate);
            await this.context.SaveChangesAsync();
        }

        public IQueryable<Alert> GetAlerts()
        {
            return this.context.Alert.Include(x => x.Group).Include(x => x.Frequency);
        }

        public IQueryable<Country> GetContries()
        {
            return this.context.Country;
        }

        public IQueryable<Language> GetLanguidges()
        {
            return this.context.Language;
        }

        public IQueryable<VerificationLevel> GetVerificationLevels()
        {
            return this.context.VerificationLevel;
        }

        public IQueryable<AlertUser> GetUserAlerts()
        {
            return this.context.AlertUser.Include($"{nameof(Alert)}.{nameof(Group)}").Include($"{nameof(Alert)}.{nameof(Frequency)}");
        }

        public IQueryable<AlertAccount> GetAccountAlerts()
        {
            return this.context.AlertAccount.Include($"{nameof(Alert)}.{nameof(Group)}").Include($"{nameof(Alert)}.{nameof(Frequency)}");
        }

        public async Task<Alert> GetAlert(string alertId)
        {
            Alert alert = await this.context.Alert.Include(x => x.Group).Include(x => x.Frequency).FirstOrDefaultAsync(x => x.AlertId == alertId);
            if (alert == null)
            {
                throw new ArgumentException($"Alert {alertId} is not exist.");
            }
            return alert;
        }

        public IQueryable<MyClient> GetMyClients(string userId)
        {
            return this.context.MyClient.Where(x => x.UserId == userId);
        }

        public async Task<AlertAccount> GetAccountAlert(string accountId, string alertId)
        {
            var alert = await this.GetAccountAlerts().FirstOrDefaultAsync(x => x.AlertId == alertId && x.AccountId == accountId);
            return alert;
        }

        public IQueryable<MyContact> GetMyContacts(string id)
        {
            return this.context.MyContact.Include(x => x.MyContactUser).Where(x => x.UserId == id);
        }

        public async Task<AlertUser> GetUserAlert(string userId, string alertId)
        {
            var alert = await this.GetUserAlerts().FirstOrDefaultAsync(x => x.AlertId == alertId && x.UserId == userId);
            return alert;
        }

        public async Task<MyContact> GetMyContact(string userId, string myContactUserId)
        {
            return await GetMyContacts(userId).FirstOrDefaultAsync(x => x.MyContactUserId == myContactUserId);
        }

        public async Task<UserProfile> GetUserProfileByEmail(string email)
        {
            return await this.context.UserProfile.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<UserProfile> GetUserProfileByPhone(string phone)
        {
            return await this.context.UserProfile.FirstOrDefaultAsync(x => x.PhoneNumber == phone);
        }

        public IQueryable<UserProfile> GetUserProfiles()
        {
            return this.context.UserProfile;
        }

        public IQueryable<Alias> GetAliases()
        {
            return this.context.Alias;
        }

        public async Task DeleteObjectsFromDb(params object[] objectsToDelete)
        {
            this.context.RemoveRange(objectsToDelete);
            await this.context.SaveChangesAsync();
        }

        public IQueryable<Guard> GetGuards()
        {
            return this.context.Guard;
        }

        public IQueryable<GuardType> GetGuardTypes()
        {
            return this.context.GuardType;
        }

        public IQueryable<File> GetFiles()
        {
            return this.context.File;
        }

        public async Task<int> EntitiesTransaction(IEnumerable<object> objectsToAdd, IEnumerable<object> objectsToUpdate,
            IEnumerable<object> objectsToDelete)
        {
            if (objectsToAdd != null)
            {
                context.AddRange(objectsToAdd);
            }

            if (objectsToUpdate != null)
            {
                context.UpdateRange(objectsToUpdate);
            }

            if (objectsToDelete != null)
            {
                context.RemoveRange(objectsToDelete);
            }

            var count = await context.SaveChangesAsync();
            return count;
        }

        public async Task DeleteObjectFromDb(object objectToDelete)
        {
            this.context.Remove(objectToDelete);
            await this.context.SaveChangesAsync();
        }

        public async Task EntitiesTransaction(TransactionObjects transactionObjects)
        {
            await EntitiesTransaction(transactionObjects.ObjectsToAdd, transactionObjects.ObjectsToUpdate, transactionObjects.ObjectsToDelete);
        }

        public async Task<UserProfile> GetUserProfilebyNickname(string nickName)
        {
            return await this.context.UserProfile.FirstOrDefaultAsync(x => x.Nickname == nickName);
        }
    }
}
