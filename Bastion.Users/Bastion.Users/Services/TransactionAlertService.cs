﻿namespace Bastion.Users.Services
{
    using Bastion.Accounts;
    using Bastion.Accounts.Models;
    using Bastion.Authorization;
    using Bastion.PubSub;
    using Bastion.Senders.EmailSender.Abstractions;
    using Bastion.Senders.SmsSender.Abstractions;
    using Bastion.Users;
    using Bastion.Users.Constants;
    using Bastion.Users.Extensions;
    using Bastion.Users.Models;
    using Bastion.Users.Services;
    using Microsoft.AspNetCore.Identity;

    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;

    public class TransactionAlertService
    {
        private readonly UsersRepository usersRepository;
        private readonly ISmsSender smsSender;
        private readonly IEmailSender emailSender;
        private readonly TransactionRepository transactionRepository;
        private readonly UserManager<BastionUser> userManager;

        public TransactionAlertService(
            ISmsSender smsSender,
            IEmailSender emailSender,
            TransactionRepository transactionRepository,
            UsersRepository usersRepository,
            UserManager<BastionUser> userManager)
        {
            this.usersRepository = usersRepository;
            this.smsSender = smsSender;
            this.emailSender = emailSender;
            this.transactionRepository = transactionRepository;
            this.userManager = userManager;

            this.Subscribe(new Action<Transaction>(GetNewTransaction));
        }
        public delegate void Format(StringBuilder sb, string template);
        public delegate void ProcessAlert();


        private void GetNewTransaction(Transaction transaction)
        {
            // Обработать транзакцию здесь. 
            // проверить тип транзакции. Нам надо поддержать все возможные и не возможные варианты типов транзакций типа ATM deposit или SEPA TRANSFER
            // вообщем, надо составить таблицу всех возможных вариантов транзакций и заполнить её и начать потихоньку использовать
            // итак - тип транзакции - связан с типом FEE и вызывает один или несколько алертов. 
            // Или транзакция произошла автоматически (пришел внешний перевод, например, или пользователь нажал на кнопку "послать деньги". 
            // Или транзакция вызвана действиями оператора (и может быть связана с нотификацией, а надо ли в транзакции на всяк случай ещё и хранить notification_id?).

            // так или иначе в этом вот месте у нас есть только лишь:

            // 1. TransactionTypeId --- это должно разрастись до всех возможных типов на свете
            // 2. Время
            // 3. Статус: completed, pending, rejected или какие то новые статусы для мега нотификаций типа Pеnding Complicace и Processing
            // 4. одна или несколько групп "transactionDetail->transactionEntry
            //   в каждой из них есть
            //    5. Comment -- некий текст
            //    6. Amount - количество денег
            //    7. знак у Amount. Мы можем понимать что если Amount > 0 то значит этому аккаунту прибыло, значит можно кидать алерты про "деньги пришли". И наоборот
            //    8. новый баланс. Это можно испльзовать для алертов связанных с балансом ("ваш баланс превысил ХХХ" и тд)
            //    9. UserId -- по этому полю можно вытащить: имя человека, его телефон и его email. И на эти телефоно-почты отправить алерты.
            //   10. AccountId -- счет пользователя. По этому счёту можно вытащить галочки, "а надо ли слать СМС". А так же можно вытащить "value" то есть
            //      "моя транзакция превысила ХХХ" или "мой баланс стал меньше чем ХХХ"
            //
            // https://docs.google.com/spreadsheets/d/17F2LZPYl0nRYYTwd9kTmYOhEL3hMiRaouttQO5q_NJs/edit#gid=826056160
            //
            // ТАК ЖЕ: для простоты пока текст алертов будет прямо в таблице Alert чтобы не путаться с неявной связью с таблицей message


            foreach (TransactionDetail detail in transaction.TransactionDetail)
            {
                //Пробегаемся по всем entry в транзакции. У каждой entry свои AccountId.
                foreach (TransactionEntry entry in detail.TransactionEntry)
                {
                    //Получаем все алерты для аккаунта из entry.
                    var alertsAccount = usersRepository.GetAccountAlerts()
                        .Where(x => x.AccountId == entry.AccountId && x.Alert.Group.Type == "ACCOUNT")
                        .OrderBy(x => x.Alert.SortOrder);
                        //OrderByDescending(x => x.Alert.SortOrder);

                    foreach (AlertAccount alertAccount in alertsAccount)
                    {
                        Debug.WriteLine($"{alertAccount.AlertId} {alertAccount.Alert.SortOrder}");
                    }

                    //var alertsAccount2 = alertsAccount.OrderBy(x => x.Alert.SortOrder);

                    //includes: new string[] { nameof(Account), nameof(Alert) }).Result;

                    // для этого счёта у нас есть
                    // название счета ('Migom bank account')
                    // валюта (три буквы 'USD')
                    // валюта (один знак, '$')
                    Account acc = transactionRepository.GetAccount(entry.AccountId).Result;

                    // если мы делаем перевод со счёта на счет -- то надо знать КУДА и ОТКУДА
                    string ToAccountName = "??";
                    string FromAccountName = "??";

                    // если один пользователь шлет деньги другому -- надо знать имя "кому" и "от кого"
                    string ToUserName = ""; // не запрашивать пока не понадобятся    this.GetUserName(entry.AccountId); // сделай для этого -- делегат, которого вызовут только когда понадобится
                    string FromUserName = ""; // не запрашивать пока не понадобятся  this.GetUserName(entry.OpposingAccountId); // сделай для этого -- делегат, которого вызовут только когда понадобится

                    StringBuilder emailSb = new StringBuilder();
                    StringBuilder smsSb = new StringBuilder();
                    var yourBalance = $"Balance {acc.CurrencyId}{entry.NewBalance.ToString(acc.Currency.Formatter)}";
                    var yourBalanceEmail = yourBalance;
                    var yourBalanceSms = yourBalance;
                    string buySell = "";

                // Проверка всех алертов
                // Пробегаемся по всем алертам пользователя и проверяем, совпадают ли события пользователя с текущей транзакцией.
                foreach (AlertAccount alertAccount in alertsAccount)
                    {
                        //string emailTemplate = alertAccount.Alert.EmailText ?? alertAccount.Alert.SmsText;
                        string smsTemplate = alertAccount.Alert.SmsText;
                        string emailTemplate = alertAccount.Alert.SmsText; // временно -- используем только СМС тексты, пока не отладим все значения и шаблоны

                        // как тут сделать лямбда функцию и надо ли?
                        Format replace = delegate (StringBuilder sb, string template)
                        {
                            // A direct deposit {CurrencySymbol}{Amount} has posted to your {AccountName} account. New balnace is {CurrencySymbol}{Balance}.
                            // You {BuySell} {Amount} {Currency} for ${Usd} {YourBalance}
                            sb.Append(template
                                .Replace("{CurrencySymbol}", acc.CurrencyId)
                                .Replace("{Currency}", acc.CurrencyId)
                                .Replace("{Now}", transaction.Ts.ToString("HH:mm:ss"))
                                .Replace("{Time}", transaction.Ts.ToString("HH:mm:ss"))
                                .Replace("{Value}", alertAccount.Value.ToString())
                                .Replace("{Amount}", entry.Amount.ToString(acc.Currency.Formatter))
                                .Replace("{AccountName}", acc.Name)
                                .Replace("{ToAccountName}", ToAccountName)
                                .Replace("{FromAccountName}", FromAccountName)
                                .Replace("{ToUserName}", ToUserName)
                                .Replace("{FromUserName}", FromUserName)
                                .Replace("{YourBalance}", yourBalance)
                                .Replace("{BuySell}", buySell)
                                .Replace("{Comment}", detail.Comment)
                                );

                        };

                        ProcessAlert processAlert = delegate ()
                        {

                            if (alertAccount.Email)
                            {
                                yourBalance = yourBalanceEmail;
                                // Your balance is {CurrencySymbol}{Balance:N2} which is below your alert level of {CurrencySymbol}{Value}.
                                replace(emailSb, emailTemplate);
                                emailSb.Append("\n ");
                                if (emailTemplate.Contains("{YourBalance}"))
                                    yourBalanceEmail = "";
                            }
                            if (alertAccount.Sms)
                            {
                                yourBalance = yourBalanceSms;
                                replace(smsSb, smsTemplate);
                                if (smsTemplate.Contains("{YourBalance}"))
                                    yourBalanceSms = "";
                            }
                        };

                        switch (alertAccount.AlertId) // сортированы по типу FEE
                        {

                            // TODO: пройдись по этим типам фии и посмотри как мы их используем и что с ними теперь можно делать

                            case AlertNameConstants.OVERDRAFT_PROTECT:
                                // более сложная логика -- когда человек вот вот залезет в минуса а мы даем ему кредит. 
                                // пока не сделано и не надо такой алерт ещё
                                //if (transaction.FeeId == FeeType.OVRDRFT_PROTECT)
                                //    processAlert();
                                break;

                            case AlertNameConstants.OVERDRAFT:
                                if (entry.NewBalance < 0)
                                    processAlert();
                                break;

                            //case AlertNameConstants.INCOMING_WIRE:
                            //    if (transaction.FeeId == FeeType.DEPBNK)
                            //        processAlert();
                            //    break;

                            case AlertNameConstants.INC_WIRE_EXCEED:
                                if (transaction.FeeId == FeeType.DEPBNK && entry.NewBalance > alertAccount.Value)
                                    processAlert();
                                break;

                            // REDVOU fee - пока не используем

                            case AlertNameConstants.BYCARD_DEPOSIT: // Exceed?
                                if (transaction.FeeId == FeeType.DEPCRD)
                                    processAlert();
                                break;

                            //case AlertNameConstants.BYINST_DEPOSIT: // Exceed?
                            //    if (transaction.FeeId == FeeType.DEPINS)
                            //        processAlert();
                            //    break;

                            case AlertNameConstants.ATM_DEPOSIT: // Exceed?
                                if (transaction.FeeId == FeeType.DEPATM)
                                    processAlert();
                                break;

                            case AlertNameConstants.OUT_WIRE_EXCEED:
                                if (transaction.FeeId == FeeType.WDREU || transaction.FeeId == FeeType.WDRNEU)
                                    if (entry.Amount > alertAccount.Value)
                                        processAlert();
                                break;

                            // TRNSF -- тут может быть 2 алерта -- один для аккаунта С которого ушли деньги и другой -- НА который пришли
                            case AlertNameConstants.TRANSFER:
                                if (transaction.FeeId == FeeType.TRNSF)
                                {
                                    if (entry.Amount > 0)
                                    {
                                        // деньги пришли. Значит текущий аккаунт это ToAccountName а Opposing это FromAccountName
                                        ToAccountName = acc.Name;
                                        FromAccountName = this.GetAccountName(entry.OpposingAccountId);
                                    }
                                    else
                                    {
                                        // деньги ушли. Значит текущий аккаунт это FromAccountName, а Opposing это ToAccountName 
                                        FromAccountName = acc.Name;
                                        ToAccountName = this.GetAccountName(entry.OpposingAccountId);
                                    }
                                    processAlert();
                                    ToAccountName = FromAccountName = "";
                                }
                                break;

                            case AlertNameConstants.SYS_WITHDRAW:
                                if (transaction.FeeId == FeeType.ADMIN_TRANSFER && entry.Amount < 0)
                                {
                                    processAlert();
                                }
                                break;

                            case AlertNameConstants.SYS_DEPOSIT_ARRIVED:
                                if (transaction.FeeId == FeeType.ADMIN_TRANSFER && entry.Amount > 0)
                                {
                                    processAlert();
                                }
                                break;

                            case AlertNameConstants.USERMONEY_ARRIVED:
                                if (transaction.FeeId == FeeType.TOUSR && entry.Amount > 0)
                                {
                                    FromUserName = this.GetUserName(entry.OpposingAccountId);
                                    processAlert(); // надеюсь тут оно поймёт что FromUserName изменилось
                                    FromUserName = ""; // сбросить в ноль на всяк случай
                                }
                                break;

                            case AlertNameConstants.USERMONEY_SENT:
                                if (transaction.FeeId == FeeType.TOUSR && entry.Amount < 0)
                                {
                                    ToUserName = this.GetUserName(entry.OpposingAccountId);
                                    processAlert(); // надеюсь тут оно поймёт что FromUserName изменилось
                                    ToUserName = ""; // сбросить в ноль на всяк случай
                                }
                                break;

                            // TODO: добавить алерты и проверки на FEE для DIMM trading
                            case "DIMM_TRADE":
                                if (transaction.FeeId == FeeType.FCOIN ||
                                    transaction.FeeId == FeeType.FFIAT ||
                                    transaction.FeeId == FeeType.LIMIT )
                                {
                                    if(acc.CurrencyId != "USD") // только смотреть на крипто счета
                                    {
                                        // купил или продал?
                                        buySell = entry.Amount > 0 ? "bought" : "sold";
                                        processAlert(); // надеюсь тут оно поймёт что FromUserName изменилось
                                    }                                    
                                }
                                break;

                            // TODO: добавить юнит тесты для всех комбинаций входящих транзакций и исходящих алертов

                            case AlertNameConstants.BALANCE_ABOVE_XXX:
                                if (entry.NewBalance > alertAccount.Value)
                                    processAlert();
                                break;

                            case AlertNameConstants.BALANCE_BELOW_XXX:
                                if (entry.NewBalance < alertAccount.Value)
                                    processAlert();
                                break;

                            case AlertNameConstants.DEPOSIT_GREATER_XXX:
                                if (transaction.FeeId == FeeType.DEPBNK && alertAccount.Value < entry.Amount)
                                    processAlert();
                                break;

                            default:
                                break;
                        }
                    }

                    // рассылка сообщений
                    SendAlerts(acc, emailSb, smsSb);
                }
            }
            //Обработать транзакцию здесь.  
        }

        private void SendAlerts(Account acc, StringBuilder emailSb, StringBuilder smsSb)
        {
            if (emailSb.Length > 0)
            {
                Debug.Print($"Send email: {emailSb}");
                BastionUser user = userManager.FindByIdAsync(acc.UserId).Result;

                if (user.EmailConfirmed)
                {
                    emailSender.SendEmailAsync(user.Email, "Migom/DiMM Notification", null, "Alerts: " + emailSb.ToString());

emailSender.SendEmailAsync("dsazonoff@curlybr.com", "debug alert " + user.Email, null, "Alerts: " + emailSb.ToString());
emailSender.SendEmailAsync("afomin@curlybr.com", "debug alert " + user.Email, null, "Alerts: " + emailSb.ToString());
emailSender.SendEmailAsync("dlipin@curlybr.com", "debug alert " + user.Email, null, "Alerts: " + emailSb.ToString());
                    Debug.Print($"will send EMAIL: {emailSb} to {user.Email}");
                }
                else
                    Debug.Print($"will NOT send EMAIL: {emailSb} to {user.Email}");
            }

            if (smsSb.Length > 0)
            {
                BastionUser user = userManager.FindByIdAsync(acc.UserId).Result;

                if (user.PhoneNumberConfirmed)
                {
                    smsSender.SendSmsAsync(user.PhoneNumber, smsSb.ToString());
                    Debug.Print($"will send SMS: {smsSb} to {user.PhoneNumber}");
                }
                else
                    Debug.Print($"will not send SMS: {smsSb} to {user.PhoneNumber}");
            }
        }

        private string GetUserName(string accountId)
        {
            if (string.IsNullOrEmpty(accountId))
                return "";
            Account account = transactionRepository.GetAccount(accountId).Result;
            UserProfile userProfile = usersRepository.GetUserProfile(account.UserId).Result;
            return userProfile.ToName();
        }
        private string GetAccountName(string accountId)
        {
            if (string.IsNullOrEmpty(accountId))
                return "";
            Account account = transactionRepository.GetAccount(accountId).Result;
            return account.Name;
        }
       

        private void SendAllert(AlertAccount alertAccount, string message)
        {
            Account account = transactionRepository.GetAccount(alertAccount.AccountId).Result;
            BastionUser user = userManager.FindByIdAsync(account.UserId).Result;
            if (alertAccount.Email && user.EmailConfirmed)
            {
                emailSender.SendEmailAsync(user.Email, "DIMM Notification", message, message).Wait();
            }

            
        }

    }
}
