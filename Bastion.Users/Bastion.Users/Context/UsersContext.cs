﻿using Microsoft.EntityFrameworkCore;

namespace Bastion.Users.Models
{
    public partial class UsersContext : DbContext
    {
        //public UsersContext()
        //{
        //}

        public UsersContext(DbContextOptions<UsersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alert> Alert { get; set; }
        public virtual DbSet<AlertAccount> AlertAccount { get; set; }
        public virtual DbSet<AlertUser> AlertUser { get; set; }
        public virtual DbSet<Alias> Alias { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<CountryRegion> CountryRegion { get; set; }
        public virtual DbSet<Extra> Extra { get; set; }
        public virtual DbSet<File> File { get; set; }
        public virtual DbSet<Frequency> Frequency { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<Guard> Guard { get; set; }
        public virtual DbSet<GuardType> GuardType { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<MyClient> MyClient { get; set; }
        public virtual DbSet<MyContact> MyContact { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<VerificationLevel> VerificationLevel { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alert>(entity =>
            {
                entity.ToTable("alert", "alert");

                entity.Property(e => e.AlertId)
                    .HasColumnName("alert_id")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultSendEmail)
                    .IsRequired()
                    .HasColumnName("default_send_email")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.DefaultSendPush)
                    .IsRequired()
                    .HasColumnName("default_send_push")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.DefaultSendSms)
                    .IsRequired()
                    .HasColumnName("default_send_sms")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(500);

                entity.Property(e => e.EmailText)
                    .HasColumnName("email_text")
                    .HasMaxLength(500);

                entity.Property(e => e.ExtraId)
                    .IsRequired()
                    .HasColumnName("extra_id")
                    .HasColumnType("character varying")
                    .HasDefaultValueSql("'NONE'::character varying");

                entity.Property(e => e.FrequencyId)
                    .IsRequired()
                    .HasColumnName("frequency_id")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'ASAP'::character varying");

                entity.Property(e => e.GroupId)
                    .IsRequired()
                    .HasColumnName("group_id")
                    .HasMaxLength(15)
                    .HasDefaultValueSql("'BANK_BALANCE'::character varying");

                entity.Property(e => e.HasValue).HasColumnName("has_value");

                entity.Property(e => e.MessageId)
                    .HasColumnName("message_id")
                    .HasMaxLength(36);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasMaxLength(20)
                    .HasComment("if this alert can only be visible to specific ASP.NET ROLE");

                entity.Property(e => e.ShowDimm)
                    .IsRequired()
                    .HasColumnName("show_dimm")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.ShowMigom)
                    .IsRequired()
                    .HasColumnName("show_migom")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.SmsText)
                    .HasColumnName("sms_text")
                    .HasMaxLength(255);

                entity.Property(e => e.SortOrder).HasColumnName("sort_order");

                entity.HasOne(d => d.Extra)
                    .WithMany(p => p.Alert)
                    .HasForeignKey(d => d.ExtraId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("alert_extra_id_fkey");

                entity.HasOne(d => d.Frequency)
                    .WithMany(p => p.Alert)
                    .HasForeignKey(d => d.FrequencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("alert_frequency_id_fkey");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Alert)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("alert_alert_group_id_fkey");
            });

            modelBuilder.Entity<AlertAccount>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.AlertId })
                    .HasName("user_alert_pkey");

                entity.ToTable("alert_account", "alert");

                entity.Property(e => e.AccountId)
                    .HasColumnName("account_id")
                    .HasMaxLength(36);

                entity.Property(e => e.AlertId)
                    .HasColumnName("alert_id")
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Push).HasColumnName("push");

                entity.Property(e => e.Sms)
                    .HasColumnName("sms")
                    .HasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("numeric");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.AlertAccount)
                    .HasForeignKey(d => d.AlertId)
                    .HasConstraintName("user_alert_alert_id_fkey");
            });

            modelBuilder.Entity<AlertUser>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.AlertId })
                    .HasName("alert_account_copy_pkey");

                entity.ToTable("alert_user", "alert");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.AlertId)
                    .HasColumnName("alert_id")
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Push).HasColumnName("push");

                entity.Property(e => e.Sms)
                    .HasColumnName("sms")
                    .HasComment("need t osend email for this type of alert?");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.AlertUser)
                    .HasForeignKey(d => d.AlertId)
                    .HasConstraintName("alert_account_copy_alert_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AlertUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("alert_account_copy_account_id_fkey");
            });

            modelBuilder.Entity<Alias>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("alias_pkey");

                entity.ToTable("alias", "profile");

                entity.HasIndex(e => e.NormalizedAlias)
                    .HasName("alias_normalized_alias_idx")
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.AliasName)
                    .IsRequired()
                    .HasColumnName("alias_name")
                    .HasMaxLength(36);

                entity.Property(e => e.Enabled).HasColumnName("enabled");

                entity.Property(e => e.NormalizedAlias)
                    .IsRequired()
                    .HasColumnName("normalized_alias")
                    .HasMaxLength(36);

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Alias)
                    .HasForeignKey<Alias>(d => d.UserId)
                    .HasConstraintName("alias_user_id_fkey");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("country", "profile");

                entity.HasIndex(e => e.RegionId);

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.FeeRegionId)
                    .HasColumnName("fee_region_id")
                    .HasMaxLength(15)
                    .HasDefaultValueSql("'NonEU'::character varying");

                entity.Property(e => e.Iso3)
                    .IsRequired()
                    .HasColumnName("iso3")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneCode).HasColumnName("phone_code");

                entity.Property(e => e.RegionId)
                    .IsRequired()
                    .HasColumnName("region_id")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Country)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("country_region_id_fkey");
            });

            modelBuilder.Entity<CountryRegion>(entity =>
            {
                entity.HasKey(e => e.RegionId)
                    .HasName("country_region_pkey");

                entity.ToTable("country_region", "profile");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Extra>(entity =>
            {
                entity.ToTable("extra", "alert");

                entity.Property(e => e.ExtraId)
                    .HasColumnName("extra_id")
                    .HasColumnType("character varying");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<File>(entity =>
            {
                entity.ToTable("file", "profile");

                entity.Property(e => e.FileId)
                    .HasColumnName("file_id")
                    .HasMaxLength(36);

                entity.Property(e => e.FileData)
                    .IsRequired()
                    .HasColumnName("file_data");

                entity.Property(e => e.FileExtension)
                    .HasColumnName("file_extension")
                    .HasMaxLength(255);

                entity.Property(e => e.FileType)
                    .IsRequired()
                    .HasColumnName("file_type")
                    .HasMaxLength(36);

                entity.Property(e => e.UploadTs)
                    .HasColumnName("upload_ts")
                    .HasColumnType("timestamp(6) without time zone")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.File)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("user_file_user_id_fkey");
            });

            modelBuilder.Entity<Frequency>(entity =>
            {
                entity.ToTable("frequency", "alert");

                entity.Property(e => e.FrequencyId)
                    .HasColumnName("frequency_id")
                    .HasMaxLength(10);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("group", "alert");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasMaxLength(15);

                entity.Property(e => e.AccountTypeId)
                    .IsRequired()
                    .HasColumnName("account_type_id")
                    .HasMaxLength(25);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.SortOrder).HasColumnName("sort_order");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'ACCOUNT'::character varying");
            });

            modelBuilder.Entity<Guard>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.GuardTypeId })
                    .HasName("user_profile_guard_pkey");

                entity.ToTable("guard", "profile");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.GuardTypeId)
                    .HasColumnName("guard_type_id")
                    .HasMaxLength(20);

                entity.Property(e => e.TwoFactor).HasColumnName("two_factor");

                entity.HasOne(d => d.GuardType)
                    .WithMany(p => p.Guard)
                    .HasForeignKey(d => d.GuardTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_profile_guard_guard_type_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Guard)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("user_profile_guard_user_id_fkey");
            });

            modelBuilder.Entity<GuardType>(entity =>
            {
                entity.ToTable("guard_type", "profile");

                entity.Property(e => e.GuardTypeId)
                    .HasColumnName("guard_type_id")
                    .HasMaxLength(20);

                entity.Property(e => e.DefaultTwoFactorRequired).HasColumnName("default_two_factor_required");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.ShowDimm).HasColumnName("show_dimm");

                entity.Property(e => e.ShowMigom).HasColumnName("show_migom");

                entity.Property(e => e.SortOrder).HasColumnName("sort_order");
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.ToTable("language", "profile");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("language_id")
                    .HasMaxLength(10);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<MyClient>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ClientId })
                    .HasName("my_client_pkey");

                entity.ToTable("my_client", "profile");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.ClientId)
                    .HasColumnName("client_id")
                    .HasMaxLength(36);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.MyClient)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("client_id_fkey");
            });

            modelBuilder.Entity<MyContact>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.MyContactUserId })
                    .HasName("my_contact_pkey");

                entity.ToTable("my_contact", "profile");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.MyContactUserId)
                    .HasColumnName("my_contact_user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.IsFavorite).HasColumnName("is_favorite");

                entity.Property(e => e.LastRequestMoney).HasColumnName("last_request_money");

                entity.Property(e => e.LastSentMoney).HasColumnName("last_sent_money");

                entity.HasOne(d => d.MyContactUser)
                    .WithMany(p => p.MyContact)
                    .HasForeignKey(d => d.MyContactUserId)
                    .HasConstraintName("my_contact_my_contact_user_id_user_profile_user_id_fkey");
            });

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("user_profile_pkey");

                entity.ToTable("user_profile", "profile");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(36);

                entity.Property(e => e.Address1)
                    .HasColumnName("address1")
                    .HasMaxLength(255);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(255);

                entity.Property(e => e.AvatarFileId)
                    .HasColumnName("avatar_file_id")
                    .HasMaxLength(36);

                entity.Property(e => e.Citizenship)
                    .HasColumnName("citizenship")
                    .HasColumnType("character varying");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(255);

                entity.Property(e => e.ClientApproved).HasColumnName("client_approved");

                entity.Property(e => e.ClientTypeId)
                    .IsRequired()
                    .HasColumnName("client_type_id")
                    .HasMaxLength(15)
                    .HasDefaultValueSql("'Individual'::character varying")
                    .HasComment("Individual, Corporate");

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(255);

                entity.Property(e => e.CountryId)
                    .HasColumnName("country_id")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.DimmFeeStructureId)
                    .HasColumnName("dimm_fee_structure_id")
                    .HasMaxLength(25)
                    .HasDefaultValueSql("'DimmDefault'::character varying");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.FeeRegionId)
                    .IsRequired()
                    .HasColumnName("fee_region_id")
                    .HasMaxLength(15)
                    .HasDefaultValueSql("'NonEU'::character varying");

                entity.Property(e => e.FeeStructureId)
                    .HasColumnName("fee_structure_id")
                    .HasMaxLength(25)
                    .HasDefaultValueSql("'debugFee'::character varying");

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(100);

                entity.Property(e => e.Inn)
                    .HasColumnName("inn")
                    .HasColumnType("character varying");

                entity.Property(e => e.KycAddressLastCountry)
                    .HasColumnName("kyc_address_last_country")
                    .HasMaxLength(255);

                entity.Property(e => e.KycAddressLastDocumentType)
                    .HasColumnName("kyc_address_last_document_type")
                    .HasMaxLength(255);

                entity.Property(e => e.KycAddressReference).HasColumnName("kyc_address_reference");

                entity.Property(e => e.KycAddressReferenceCreationTime)
                    .HasColumnName("kyc_address_reference_creation_time")
                    .HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.KycAddressStatus)
                    .HasColumnName("kyc_address_status")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'none'::character varying")
                    .HasComment("none, pending, rejected, aprove");

                entity.Property(e => e.KycCompleted).HasColumnName("kyc_completed");

                entity.Property(e => e.KycIdentityReference).HasColumnName("kyc_identity_reference");

                entity.Property(e => e.KycIdentityReferenceCreationTime)
                    .HasColumnName("kyc_identity_reference_creation_time")
                    .HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.KycIdentityStatus)
                    .HasColumnName("kyc_identity_status")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'none'::character varying")
                    .HasComment("none, pending, rejected, aprove");

                entity.Property(e => e.Visible).HasColumnName("visible");

                entity.Property(e => e.UnlimitedStatus)
                    .HasColumnName("unlimited_status")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'none'::character varying")
                    .HasComment("none, pending, rejected, approved");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("language_id")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'en'::character varying");

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(100);

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(255);

                entity.Property(e => e.Nationality)
                    .HasColumnName("nationality")
                    .HasMaxLength(255);

                entity.Property(e => e.Nickname)
                    .HasColumnName("nickname")
                    .HasMaxLength(255);

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasMaxLength(255);

                entity.Property(e => e.PostCode)
                    .HasColumnName("post_code")
                    .HasMaxLength(15);

                entity.Property(e => e.PromoFeeExpireDate)
                    .HasColumnName("promo_fee_expire_date")
                    .HasColumnType("date")
                    .HasComment("we must set this to some future date WHEN we apply promo code to user");

                entity.Property(e => e.RegistrationTs)
                    .HasColumnName("registration_ts")
                    .HasColumnType("timestamp(6) without time zone")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.RegistrationType)
                    .HasColumnName("registration_type")
                    .HasMaxLength(255)
                    .HasComment("Web, Swig and etc");

                entity.Property(e => e.SendMoneyReference).HasColumnName("send_money_reference");

                entity.Property(e => e.Sex)
                    .HasColumnName("sex")
                    .HasColumnType("character varying");

                entity.Property(e => e.Ssn)
                    .HasColumnName("ssn")
                    .HasMaxLength(255);

                entity.Property(e => e.StateOrProvince)
                    .HasColumnName("state_or_province")
                    .HasMaxLength(255);

                entity.Property(e => e.TimeZone)
                    .HasColumnName("time_zone")
                    .HasMaxLength(255);

                entity.Property(e => e.VerificationLevelId).HasColumnName("verification_level_id");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.UserProfile)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("user_profile_country_id_fkey");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.UserProfile)
                    .HasForeignKey(d => d.LanguageId)
                    .HasConstraintName("user_profile_language_id_fkey");

                entity.HasOne(d => d.VerificationLevel)
                    .WithMany(p => p.UserProfile)
                    .HasForeignKey(d => d.VerificationLevelId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("user_profile_verification_level_id_fkey");
            });

            modelBuilder.Entity<VerificationLevel>(entity =>
            {
                entity.ToTable("verification_level", "profile");

                entity.Property(e => e.VerificationLevelId)
                    .HasColumnName("verification_level_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DescriptionApproved)
                    .HasColumnName("description_approved")
                    .HasMaxLength(255);

                entity.Property(e => e.DescriptionNone)
                    .HasColumnName("description_none")
                    .HasMaxLength(255);

                entity.Property(e => e.DescriptionPending)
                    .HasColumnName("description_pending")
                    .HasMaxLength(255);

                entity.Property(e => e.DescriptionRejected)
                    .HasColumnName("description_rejected")
                    .HasMaxLength(255);

                entity.Property(e => e.MaxLimit)
                    .HasColumnName("max_limit")
                    .HasColumnType("numeric(10,2)");

                entity.Property(e => e.MaxLimitDimm)
                    .HasColumnName("max_limit_dimm")
                    .HasColumnType("numeric(10,0)");

                entity.Property(e => e.MaxLimitMigom)
                    .HasColumnName("max_limit_migom")
                    .HasColumnType("numeric(10,0)");

                entity.Property(e => e.ShortTitle)
                    .HasColumnName("short_title")
                    .HasMaxLength(255);

                entity.Property(e => e.TitleApproved)
                    .HasColumnName("title_approved")
                    .HasMaxLength(255);

                entity.Property(e => e.TitleNone)
                    .HasColumnName("title_none")
                    .HasMaxLength(255);

                entity.Property(e => e.TitlePending)
                    .HasColumnName("title_pending")
                    .HasMaxLength(255);

                entity.Property(e => e.TitleRejected)
                    .HasColumnName("title_rejected")
                    .HasMaxLength(255);
            });
        }
    }
}
